import http from './httpAxios'

export default {
    page(params){
        return http.get('/constant-category',params);
    },
    listConstantType(){
        return http.get('/constant-category/type');
    },
    saveConstantCategory(params){
        return http.post('/constant-category',params);
    },
    editById(params){
        return http.put('/constant-category',params);
    },
    delById(id){
        return http.del(`/constant-category/${id}`);
    },
    listByConstantCode(type){
        return http.get(`constant-category/${type}`)
    }
}