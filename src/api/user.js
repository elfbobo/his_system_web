import http from '@/api/httpAxios'

export default {
    captcha(){
        return http.get('/user/captcha');
    },
    login(userInfo){
        return http.post('/user/login',userInfo);
    },
    page(params){
        return http.get('/user',params);
    },
    save(params){
        return http.post('/user',params);
    },
    editById(params){
        return http.put('/user',params);
    },
    delById(id){
        return http.del(`/user/${id}`);
    },
    exist(relName){
        return http.get(`/user/exist/${relName}`)
    },
    reload(id){
        return http.post(`/user/reload/${id}`);
    }
}