import http from '@/api/httpAxios'

export default {
    page(params){
        return http.get('/drugs',params);
    },
    save(params){
        return http.post('/drugs',params);
    },
    editById(params){
        return http.put('/drugs',params);
    },
    delById(id){
        return http.del(`/drugs/${id}`);
    },
    importExcel(params) {
        return http.upload('/drugs/fileUpload', params);
    },
    exportExcel(){
        return http.get('/drugs/downLoad');
    }
}