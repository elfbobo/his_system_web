import constantCategory from "@/api/constantCategory";
import user from '@/api/user';
import office from '@/api/office';
import drugs from '@/api/drugs';

export default {
    constant: constantCategory,
    user,
    office,
    drugs
}