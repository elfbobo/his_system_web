import axios from 'axios'  //异步请求
import qs from 'qs' //用于post请求的数据转换
import router from "@/router" //页面跳转 replace  push
//elementUI中的按需加载 ：Message用于消息提示，Loading 请求加载层
import {Loading,Message} from 'element-ui'

// axios 全局配置
axios.defaults.timeout = 5000; // 5s没响应则认为该请求失败
// 配置请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
axios.defaults.baseURL = '/api';  //跨域配置可以添加上它，就不用每一个请求都以api开头了

let loadingInstance; //加载对象，设置为全局的对象
// 添加请求拦截器
axios.interceptors.request.use(request => {
    //TODO 开始请求，日志打印
    console.log("开始请求:",request.url,"，参数：",JSON.stringify(request.params));

    //加载层：数据加载中。。
    loadingInstance = Loading.service({
        lock: true,
        text: '数据加载中，请稍后...',
        spinner: 'el-icon-loading',
        background: 'rgba(0, 0, 0, 0.7)'
    });

    //如果是post 请求，需要做数据转换
    if (request.method === 'post' || request.method === 'put') {
        //如果是文件上传，则不需要进行数据装换
        //约定，如果是文件上传，则请求地址以 fileUpload 结尾
        let currUrl = request.url.split("/")[request.url.split("/").length-1];
        if(currUrl !== 'fileUpload'){
            console.log('---非文件上传');
            request.data = qs.stringify(request.data)  //这里使用qs对data就行处理
        }
    }

    if(request.method === 'get'){
        //如果是下载，需要指定下载格式为 blob 或者 二进制
        //约定，如果是文件下载，则请求地址以 downLoad 结尾
        let curUrl = request.url.split("/")[request.url.split("/").length-1];
        if(curUrl === 'downLoad'){
            request.responseType='blob';
            return request; //这里对上传文件的 api 不做传参序列化处理
        }
    }

    return request;
},err => {
    loadingInstance.close();
    Message.error('请求超时!');
    return Promise.reject(err)
}
);
//添加响应拦截器
axios.interceptors.response.use(response => {
    //TODO 请求完毕数据输出,后续添加后端返回的对应状态判断
    console.log("请求完毕数据：",response.data);
    loadingInstance.close(); //关闭加载层

    //把下载的 blob 数据转为 excel
    if(response.config.responseType === 'blob'){  //下载excel类型
        console.log('--------');
        let blob = new Blob([response.data], {type: 'application/vnd.ms-excel;charset=utf-8'});
        let downloadElement = document.createElement('a');
        let href = window.URL.createObjectURL(blob); //创建下载的链接
        downloadElement.href = href;
        downloadElement.download = decodeURI(response.headers['content-disposition'].split("=")[1]);  //处理文件名乱码问题; //下载后文件名
        document.body.appendChild(downloadElement);
        downloadElement.click(); //点击下载
        document.body.removeChild(downloadElement); //下载完成移除元素
        window.URL.revokeObjectURL(href); //释放掉blob对象
        return;
    }

    if (response.data.code === 200) {
        return response.data
    }else if (response.data.code === 504) {
        Message.error('服务器被吃了⊙﹏⊙∥');
    } else if(response.data.code === 404){
        Message.error('请求地址不存在!');
        router.replace({path:'/404'})
    }else if (response.data.code === 403) {
        Message.error('权限不足,请联系管理员!');
        router.replace({path:'/403'})
    } else if (response.data.code === 401) { //未登录
        // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
        router.replace({
            path: '/login',
            query: {
                redirect: router.currentRoute.fullPath
            }
        });
    }else{
        //Message.error(response.data.msg);
    }
    return response.data
},err => {
    loadingInstance.close();
    Message.error('请求失败，请稍后再试');
    return Promise.reject(err)
}
);

//对 axios 的所有请求进行参数统一规格
let http = {
    get(url, params = {}){
        return axios.get(url, {params})
    },
    post(url, params = {}){
        return axios.post(url, params)
    },
    del(url, params = {}){
        return axios.delete(url, {params})
    },
    put(url, params = {}){
        return axios.put(url, params)
    },
    upload(url, params = {}){
        return axios.post(url, params,{
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    }
};

export default http;


