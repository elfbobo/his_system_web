import http from './httpAxios'

export default {
    page(params){
        return http.get('/office',params);
    },
    save(params){
        return http.post('/office',params);
    },
    editById(params){
        return http.put('/office',params);
    },
    delById(id){
        return http.del(`/office/${id}`);
    },
    list(){
        return http.get('/office/list');
    }
}