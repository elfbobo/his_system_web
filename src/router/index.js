import Vue from 'vue';
import Router from 'vue-router';

//基础界面
import Home from '@/components/Home'
import Login from '@/views/Login'
import Welcome from '@/views/Welcome'
import E404 from '@/views/404'
import E403 from '@/views/403'

//功能界面
import ConstantCategory from '@/views/base/ConstantCategory'
import Office from '@/views/base/Office'
import User from '@/views/base/User'
import Drugs from '@/views/base/Drugs'

Vue.use(Router);

const routes = [
    {name: '默认页面',path: '/',redirect: '/login'},
    {name: '首页',path: '/',component: Home, children: [
        {name:'欢迎界面',path: '/welcome',component: Welcome},
        {name:'404错误页面',path: '/404',component: E404},
        {name:'403错误页面',path: '/403',component: E403},
         //组件路由配置规则
        {name:'常数类别',path: '/constantCategory',component: ConstantCategory},
        {name:'科室管理',path: '/office',component: Office},
        {name:'用户管理',path: '/user',component: User},
        {name:'非药品收费项目管理',path: '/drugs',component: Drugs},
    ]},
    {name:'登录',path: '/login',component: Login},
    {name:'其他请求',path: '*',redirect: '/404'}
];

let router = new Router({
    mode: 'history',
    routes
});

export default router;