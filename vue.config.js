module.exports = {
    devServer: {
      proxy: {
        '/api': { //代理以 api 开头的请求
          target: 'http://127.0.0.1:80/', //当前请求地址代理到配置的目标地址上
          ws: true,              // 是否启用websockets
          changeOrigin: true,    //开启代理
          secure: false,       // 将安全设置为false,才能访问https开头的
          pathRewrite: {
              '^/api': '' //api 开头的地址 替换为 '' 空开头，即去除 api 
          }
        }
      }
    }
}